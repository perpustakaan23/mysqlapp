# mysqlapp



Judul Aplikasi : Aplikasi Perpustakaan

Basisdata atau framework yang digunakan:
tabel customers dan borrows : MySQL dan Flask
tabel books: MongoDB dan FastAPI

Authentication: JWT Token



Fungsi

Customers
fungsi dan route-nya :
	Melihat user -> /users
	Melihat user by id -> /user
	input data user ->/user/insert
	ubah data user -> /user/update
	hapus data user -> /user/delete
    request token -> /user/requesttoken



Borrow
Fungsi dan route-nya:
	Melihat daftar peminjaman untuk user id tertentu -> /borrows
    (input token yang telah didaftarkan pada authentication)
	Menambahkan daftar pinjaman -> /borrows/insert
	Ubah status pinjaman (aktif-> nonaktif) -> /borrows/status